### 1. Simple Database querying

```sql

SELECT
	a."UserName" as "UserName",
	(
		SELECT "UserName"
		FROM "USER" as b
		WHERE a."Parent" = b."ID"
	) as ParentUserName
FROM (
	SELECT "ID", "UserName", "Parent"
	FROM "USER"
) as a


``` 

### 2. OMDBAPI

[Repo - simple express OMDBAPI](https://gitlab.com/genjerdotkom/clean-architecture-typescript-omdbapi)


### 3. Refactor Code

```js

function findFirstStringInBracket(str){
    return str.match(/\(([^)]+)\)/)[1]
}


```

### 4. Anagrams

```js

function simpleAnagrams() {
    let arr = ['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua'];
    let anagrams = {}
    let results = []
    for(let i = 0; i < arr.length; i++){
        let sorted = arr[i].split('').sort().join('')
        anagrams[sorted] = anagrams[sorted] || []
        anagrams[sorted].push(arr[i])
    }
    for (let key in anagrams){
        results.push(anagrams[key])
    }
    return results
}


```

GinanjarDP
[Linkeddin](https://id.linkedin.com/in/ginanjar-putranto-0416a913b)
